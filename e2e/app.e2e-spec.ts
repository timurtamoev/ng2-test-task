import { Ng222222Page } from './app.po';

describe('ng222222 App', () => {
  let page: Ng222222Page;

  beforeEach(() => {
    page = new Ng222222Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
