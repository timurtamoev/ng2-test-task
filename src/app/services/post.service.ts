import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PostService {
  options;

  constructor(
    private http: Http
  ) { }

  getData (url: string) {
    return this.http.get(url).map(res => res.json())
  }

}
