import {Component} from '@angular/core';
import {PostService} from './services/post.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  url = "http://37.252.66.66/tw/?hashtag=";
  hashtag: string = '';
  public data: any = [];

  constructor(
    private postService: PostService) {
  }

  onSubmit() {
    this.postService.getData(this.url + this.hashtag).subscribe(
      data => {
        this.data = data.statuses
      },
      error => {
        console.log(error)
      });
  }
}

